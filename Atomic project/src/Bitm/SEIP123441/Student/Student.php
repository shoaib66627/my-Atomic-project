<?php

namespace App\Bitm\SEIP123441\Student;
use PDO;
class Student
{
    public $conn = "";
    public $user = "root";
    public $servername = "localhost";
    public $pass = "";
    public $database = "student";
    public $firstname = "";
    public $lastname = "";
    public $password = "";
    public $mobile = "";


    public function __construct()
    {
        try {
            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->database", $this->user, $this->pass);
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo "Connected successfully";
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    /**
     * @return PDO|string
     */
    public function setData($data="")
    {
        if(array_key_exists('firstname',$data)and !empty($data)){
            $this->firstname=$data['firstname'];
        };
        if(array_key_exists('lasname',$data)and !empty($data)){
            $this->lastname=$data['lastname'];
        };
        if(array_key_exists('mobile',$data)and !empty($data)){
            $this->mobile=$data['mobile'];
        };
        return $this;
    }
    public function Store(){
        $query="INSERT INTO `signup` ( `first_name`, `last_name`, `mobile_number`) VALUES (:first, :last, :mobile)";
        $stmt=$this->conn->prepare( $query);
        $result=$stmt->execute(array(':first'=>$this->firstname,
            ':last'=>$this->lastname,'mobile'=>$this->mobile
        ));
    }
    
    public function index(){
       $sqlquery="SELECT * FROM `signup`";
        $stmt=$this->conn->query($sqlquery);
        $alldata=$stmt->fetchAll(PDO::FETCH_ASSOC);
        return $alldata;
    }

}
?>